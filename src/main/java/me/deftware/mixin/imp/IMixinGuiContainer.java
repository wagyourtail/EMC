package me.deftware.mixin.imp;

import net.minecraft.screen.slot.Slot;

public interface IMixinGuiContainer {

    Slot getHoveredSlot();

}
